import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from "axios";

const bitmexURL = "https://www.bitmex.com/api/v1/orderBook/L2?symbol=XBT&depth=10";
const bitfinexURL = "https://api.bitfinex.com/v1/book/btcusd?limit_bids=10&limit_asks=10";

class App extends React.Component {

    state = {
        bitmexDocs: [],
        bitfiniexDocs: [],
    };

    componentDidMount() {
        // fetch(bitfinexURL, {
        //     method: "GET",
        //     headers: {
        //         "Access-Control-Allow-Origin": "*",
        //         "Content-Type": "application/json"
        //     }
        // })

        // axios.get(bitfinexURL, {
        //     headers: {
        //                 "Access-Control-Allow-Origin": "*",
        //                 "Content-Type": "application/json"
        //             }
        // })

        const instance = axios.create({
            headers: {
                "Access-Control-Allow-Origin": "https://api.bitfinex.com"
            }
        });
        instance.get(bitfinexURL)
            .then((res) => res.text())
            .then((data) => this.setState({ bitfinexDocs: data }))
            .catch(error => {
                console.log("====>", error);
            });
    }


    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={ logo } className="App-logo" alt="logo"/>
                    <p>
                        { this.state.bitfiniexDocs }
                    </p>
                </header>
            </div>
        )
    };
}

export default App;
